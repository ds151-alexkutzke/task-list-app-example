import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, FlatList } from 'react-native';

const TodoListScreen = () => {
  const [tasks, setTasks] = useState([]);
  const [newTask, setNewTask] = useState('');

  function addNewTask(newTaskTitle) {
    if (!newTask) return;

    let lastId = 0;
    
    tasks.forEach(task => {
      if (lastId < task.id) {
        lastId = task.id;
      }
    });

    setTasks([...tasks, { id: ++lastId, title: newTaskTitle, done: false }]);
    setNewTask('')
  }

  function changeTaskStatus(selectedTaskId) {
    let newTasks = tasks.map(task => {
      if (task.id === selectedTaskId) {
        const newObject = Object.assign({}, task)
        newObject.done = !task.done;
        return(newObject);
      }
      else{
        return(task);
      }
    });

    setTasks(newTasks);
  }
  
  const notDoneTasks = tasks.filter((task) => !task.done );
  const doneTasks = tasks.filter((task) => task.done );

  return (
    <View>
      <TextInput
        style={styles.textInput}
        placeholder="Nova Tarefa"
        autoFocus={true}
        autoCapitalize="sentences"
        autoCorrect={true}
        value={newTask}
        onChangeText={(t) => setNewTask(t)}
        onEndEditing={() => addNewTask(newTask)}
        onKeyPress={({ key }) => key === 'Enter' ? addNewTask(newTask) : null}
      />
      <FlatList
        data={[...notDoneTasks, ...doneTasks]}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => {
          return (
            <Text
              style={[styles.task, !item.done ? styles.taskNotDone : styles.taskDone]}
              onPress={() => changeTaskStatus(item.id)}
            >
              {item.title}
            </Text>
          )
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  textInput: {
    margin: 10,
    padding: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    fontSize: 16,
  },
  task: {
    fontSize: 16,
    margin: 10,
    padding: 10,
  },
  taskDone: {
    backgroundColor: 'gray',
    textDecorationLine: 'line-through',
  },
  taskNotDone: {
    backgroundColor: 'yellow',
    textDecorationLine: 'none',
  },
});

export default TodoListScreen;

