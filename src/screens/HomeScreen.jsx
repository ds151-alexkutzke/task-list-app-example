import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

const HomeScreen = ({ navigation }) => {
  return (
    <View>
      <Button
        title="Vai para Todo List"
        onPress={() => navigation.navigate('TodoList')}
      />
    </View>
  )
}

const styles = StyleSheet.create({});

export default HomeScreen;
